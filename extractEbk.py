#!/usr/bin/env python3
import zipfile,rarfile
import re
import sys,os,os.path


ptnEbkFile = re.compile(r"/*(.*)(\.(epub|pdf))$",re.IGNORECASE)
print("Pattern groups: %d" % ptnEbkFile.groups)

def processZip(zipPath):
    parentDir = os.path.dirname(zipPath)
    print("Parent DIR: %s" % parentDir)
    zipMainFileName = re.search(r"(.*)(.zip)$",os.path.basename(zipPath),re.IGNORECASE).group(1)
    print("Zip Main File name: %s" % zipMainFileName)

    zip = zipfile.ZipFile(zipPath)
    for info in zip.infolist():
        if not info.is_dir():
            print("Name = %s" % info.filename)
            if ptnEbkFile.search(info.filename):
                ebkExtFileName = ptnEbkFile.search(info.filename).group(2)
                print( "\tMainFileName: %s" % ptnEbkFile.search(info.filename).group(1) )
                print( "\tExtFileName: %s" % ebkExtFileName )

                #extract ebk file
                newFilePath = os.path.join(parentDir, zipMainFileName+ebkExtFileName)
                print("EXTRACT: %s --> %s" % (info.filename, newFilePath) )
                with open(newFilePath,'wb') as fout:
                    fout.write( zip.read(info) )

    zip.close()

def processRar(rarPath):
    parentDir = os.path.dirname(rarPath)
    print("Parent DIR: %s" % parentDir)
    rarMainFileName = re.search(r"(.*)(.rar)$",os.path.basename(rarPath),re.IGNORECASE).group(1)
    print("Rar Main File name: %s" % rarMainFileName)

    rar = rarfile.RarFile(rarPath)
    for info in rar.infolist():
        if not info.isdir():
            print("Name = %s" % info.filename)
            if ptnEbkFile.search(info.filename):
                ebkExtFileName = ptnEbkFile.search(info.filename).group(2)
                print( "\tMainFileName: %s" % ptnEbkFile.search(info.filename).group(1) )
                print( "\tExtFileName: %s" % ebkExtFileName )

                #extract ebk file
                newFilePath = os.path.join(parentDir, rarMainFileName+ebkExtFileName)
                print("EXTRACT: %s --> %s" % (info.filename, newFilePath) )
                with open(newFilePath,'wb') as fout:
                    fout.write( rar.read(info) )


if __name__=="__main__":

    if( len(sys.argv)<=1 ):
        print("Please provide path argument!! ")
        sys.exit()

    if( not os.path.exists(sys.argv[1]) ):
        print("Path %s not exist!!" % sys.argv[1])
        sys.exit()
    

    try:
        if re.search(".*(.zip)$",sys.argv[1],re.IGNORECASE) :
            processZip(sys.argv[1])
        elif re.search(".*(.rar)$",sys.argv[1],re.IGNORECASE) :
            processRar(sys.argv[1])
        else:
            raise Exception("not supported input file type!!")
    except:
        print( "Error occurred!! \n \t%s \n\t%s" % (sys.exc_info()[0],sys.exc_info()[1]) )
